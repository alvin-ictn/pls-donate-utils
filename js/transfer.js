const noblox = require("noblox.js");
const child = require("child_process");
const fs = require("fs");
const config = require("../config.json");
const term = require("terminal-kit").terminal;
const file = JSON.parse(fs.readFileSync("./accounts.json"));
const fetch = require("node-fetch");
let user;
let shirtId;
let productId;

async function getToken(COOKIE) {
	let xCsrfToken = "";
	const rbxRequest = async (verb, url, body) => {
		const response = await fetch(url, {
			headers: {
				Cookie: `.ROBLOSECURITY=${COOKIE};`,
				"x-csrf-token": xCsrfToken,
				"Content-Length": body?.length.toString() || "0",
			},
			method: "POST",
			body: body || "",
		});
		if (response.status == 403) {
			if (response.headers.has("x-csrf-token")) {
				xCsrfToken = response.headers.get("x-csrf-token");
				return rbxRequest(verb, url, body);
			}
		}
		return response;
	};
	const response = await rbxRequest("POST", "https://auth.roblox.com");
	return xCsrfToken;
}

async function transfer(x) {
	if (x.Username == user.Username) return;
	let currentUser;
	let balance;
	try {
		currentUser = await noblox.setCookie(x.Cookie);
		balance = currentUser.RobuxBalance;
		console.log(x.Username + ": " + balance);
	} catch (e) {
		console.log("\nrate limited, waiting 60 seconds...");
		await new Promise((r) => setTimeout(r, 60000));
		currentUser = await noblox.setCookie(x.Cookie);
		balance = currentUser.RobuxBalance;
		console.log(x.Username + ": " + balance);
	}
	if (balance >= 5) {
		try {
			await noblox.deleteFromInventory(shirtId);
		} catch (e) {}
		child.execSync(`node ./js/product.js ${user.Username} ${balance} ${shirtId}`, { stdio: "inherit" });
		await fetch(`https://economy.roblox.com/v1/purchases/products/${productId}`, {
			method: "POST",
			headers: {
				Cookie: `.ROBLOSECURITY=${x.Cookie};`,
				"x-csrf-token": await getToken(x.Cookie),
				"Content-Type": "application/json",
				Accept: "application/json",
			},
			body: JSON.stringify({ expectedCurrency: 1, expectedPrice: balance }),
		});
	}
}

(async () => {
	console.log("Enter the user to transfer to ");
	let arg1 = await term.inputField().promise;
	console.log("\nEnter the T-SHIRT ID to use ");
	shirtId = await term.inputField().promise;
	console.log("\n");
	user = file.filter(function (acc) {
		return acc.Username == arg1;
	})[0];
	if (user) {
		let req = await fetch("https://catalog.roblox.com/v1/catalog/items/details", {
			method: "POST",
			headers: {
				Cookie: `.ROBLOSECURITY=${user.Cookie};`,
				"x-csrf-token": await getToken(user.Cookie),
				"Content-Type": "application/json",
				Accept: "application/json",
			},
			body: JSON.stringify({
				items: [
					{
						itemType: "Asset",
						id: shirtId,
					},
				],
			}),
		});
		req = await req.json();
		productId = req.data[0].productId;

		for (let x of file) {
			try {
				await transfer(x);
			} catch (e) {
				console.log("\nrate limited, waiting 60 seconds...");
				await new Promise((r) => setTimeout(r, 60000));
				await transfer(x);
			}
		}
	} else {
		console.log("user not found");
	}
	process.exit();
})();
