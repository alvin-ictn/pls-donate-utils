const fetch = require("node-fetch");
async function getToken(COOKIE) {
  let xCsrfToken = "";
  const rbxRequest = async (verb, url, body) => {
    const response = await fetch(url, {
      headers: {
        Cookie: `.ROBLOSECURITY=${COOKIE};`,
        "x-csrf-token": xCsrfToken,
        "Content-Length": body?.length.toString() || "0",
      },
      method: "POST",
      body: body || "",
    });
    if (response.status == 403) {
      if (response.headers.has("x-csrf-token")) {
        xCsrfToken = response.headers.get("x-csrf-token");
        return rbxRequest(verb, url, body);
      }
    }
    return response;
  };
  const response = await rbxRequest("POST", "https://auth.roblox.com");
  console.log(response);
  return xCsrfToken;
}

const test = getToken()
console.log("test")